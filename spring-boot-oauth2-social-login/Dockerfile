# Stage 1: Build the Spring Boot application
FROM maven:3.8.4-openjdk-11 AS build
WORKDIR /app
# Copy the Maven project definition and build dependencies
COPY pom.xml .
RUN mvn clean package -DskipTests
# Copy the source code
COPY src src
# Build the Spring Boot application
RUN mvn package -DskipTests
# Stage 2: Create a production image
FROM openjdk:11-jre-slim
# Set the working directory inside the container
WORKDIR /app
# Copy the JAR file from the previous build stage
COPY --from=build /app/target/*.jar ./app.jar
# Expose the port that your Spring Boot application is listening on
EXPOSE 8080
# Define the command to run your Spring Boot application
CMD ["java", "-jar", "app.jar"]
